package fr.bakaaless.AvatarReturnsBot.Spigot.messages;

import fr.bakaaless.AvatarReturnsBot.Spigot.plugin.SpigotMain;
import lombok.Getter;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class OutgoingMessages {

    @Getter
    private final SpigotMain main;

    @Getter
    private final String channel;

    public OutgoingMessages(final SpigotMain main) {
        this.main = main;
        this.channel = this.getMain().getPluginMessage().getChannel();
        this.getMain().getServer().getMessenger().registerOutgoingPluginChannel(this.getMain(), this.getChannel());
    }

    public void verifyDiscordUser(final Player player) {
        try {
            final ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
            final DataOutputStream out = new DataOutputStream(byteArrayOut);
            out.writeUTF("verifyDiscordUser");
            out.writeUTF(player.getName());
            player.sendPluginMessage(this.getMain(), this.getChannel(), byteArrayOut.toByteArray());
        } catch (Exception ignored) {
        }
    }

    public void createFaction(final Player player, final String name) {
        try {
            final ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
            final DataOutputStream out = new DataOutputStream(byteArrayOut);
            out.writeUTF("createFaction");
            out.writeUTF(name);
            out.writeUTF(player.getName());
            player.sendPluginMessage(this.getMain(), this.getChannel(), byteArrayOut.toByteArray());
        } catch (Exception ignored) {
        }
    }

    public void changeName(final Player player, final String oldName, final String newName) {
        try {
            final ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
            final DataOutputStream out = new DataOutputStream(byteArrayOut);
            out.writeUTF("renameFaction");
            out.writeUTF(oldName);
            out.writeUTF(newName);
            player.sendPluginMessage(this.getMain(), this.getChannel(), byteArrayOut.toByteArray());
        } catch (Exception ignored) {
        }
    }

    public void deleteFaction(final Player player, final String name) {
        try {
            final ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
            final DataOutputStream out = new DataOutputStream(byteArrayOut);
            out.writeUTF("disbandFaction");
            out.writeUTF(name);
            player.sendPluginMessage(this.getMain(), this.getChannel(), byteArrayOut.toByteArray());
        } catch (Exception ignored) {
        }
    }

    public void joinFaction(final Player player, final String name) {
        try {
            final ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
            final DataOutputStream out = new DataOutputStream(byteArrayOut);
            out.writeUTF("joinFaction");
            out.writeUTF(name);
            out.writeUTF(player.getName());
            player.sendPluginMessage(this.getMain(), this.getChannel(), byteArrayOut.toByteArray());
        } catch (Exception ignored) {
        }
    }

    public void leaveFaction(final Player player, final String name) {
        try {
            final ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
            final DataOutputStream out = new DataOutputStream(byteArrayOut);
            out.writeUTF("leaveFaction");
            out.writeUTF(name);
            out.writeUTF(player.getName());
            player.sendPluginMessage(this.getMain(), this.getChannel(), byteArrayOut.toByteArray());
        } catch (Exception ignored) {
        }
    }

    public void kickFaction(final Player player, final String kicked, final String name) {
        try {
            final ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
            final DataOutputStream out = new DataOutputStream(byteArrayOut);
            out.writeUTF("kickFaction");
            out.writeUTF(name);
            out.writeUTF(kicked);
            player.sendPluginMessage(this.getMain(), this.getChannel(), byteArrayOut.toByteArray());
        } catch (Exception ignored) {
        }
    }
}
