package fr.bakaaless.AvatarReturnsBot.Spigot.commands.blacklist;

import fr.bakaaless.AvatarReturnsBot.Spigot.mysql.DBConnection;
import org.bukkit.command.CommandSender;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

class Remove {

    static boolean handle(final CommandSender sender, final String[] args) {
        if (!sender.hasPermission("avatarbot.blacklist.remove")) {
            sender.sendMessage("§c§lAvatarBot §4§l» §cVous n'avez pas accès à cette commande.");
            return false;
        }
        if (!DBConnection.isOpen()) {
            sender.sendMessage("§c§lAvatarBot §4§l» §cLa base de données n'est pas disponible.");
            return false;
        }
        try {
            final UUID uuid = UUID.fromString(args[1]);
            try (final ResultSet resultSet = DBConnection.sql.readQuery("SELECT * FROM `blacklist` WHERE `uuid` = '" + uuid.toString() + "'")) {
                while (resultSet.next()) {
                    DBConnection.sql.modifyQuery("DELETE FROM `blacklist` WHERE `uuid` = '" + uuid.toString() + "'", true);
                    sender.sendMessage("§3§lAvatarBot §8§l» §7L'UUID §c" + uuid.toString() + " §7a été supprimée de la blacklist.");
                    resultSet.close();
                    return true;
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            sender.sendMessage("§c§lAvatarBot §4§l» §cCette uuid n'est pas sur la blacklist.");
            return false;
        } catch (Exception e) {
            sender.sendMessage("§c§lAvatarBot §4§l» §cVous n'avez pas entré d'uuid.");
            return false;
        }
    }
}
