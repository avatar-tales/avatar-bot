package fr.bakaaless.AvatarReturnsBot.Spigot.commands.blacklist;

import fr.bakaaless.AvatarReturnsBot.Spigot.mysql.DBConnection;
import fr.bakaaless.AvatarReturnsBot.Spigot.plugin.SpigotMain;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.UUID;

class Add {

    static boolean handle(final CommandSender sender, final String[] args) {
        if (!sender.hasPermission("avatarbot.blacklist.add")) {
            sender.sendMessage("§c§lAvatarBot §4§l» §cVous n'avez pas accès à cette commande.");
            return false;
        }
        if (!DBConnection.isOpen()) {
            sender.sendMessage("§c§lAvatarBot §4§l» §cLa base de données n'est pas disponible.");
            return false;
        }
        try {
            final UUID uuid = UUID.fromString(args[1]);
            final Calendar calendar = Calendar.getInstance();
            try (final ResultSet resultSet = DBConnection.sql.readQuery("SELECT * FROM `blacklist` WHERE `uuid` = '" + uuid.toString() + "'")) {
                while (resultSet.next()) {
                    calendar.setTimeInMillis(resultSet.getLong("date"));
                    sender.sendMessage("§c§lAvatarBot §4§l» §cCe joueur est déjà sur la blacklist depuis le : §4" + SpigotMain.getString(calendar));
                    resultSet.close();
                    return false;
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            DBConnection.sql.modifyQuery("INSERT INTO `blacklist` (`uuid`,`date`, `staff`) VALUES ('" + uuid + "'," + calendar.getTimeInMillis() + ", '" + sender.getName() + "')", true);
            for (final Player player : SpigotMain.getInstance().getServer().getOnlinePlayers()) {
                if (player.getUniqueId().equals(uuid)) {
                    player.kickPlayer("§8§l- §c§lAvatar Bot §8§l-\n\n§7Vous ne vous êtes pas enregistré avec\nvotre compte discord pourtant\nvous avez pu entrer.\n\n§c§lDate §8§l» §7" + SpigotMain.getString(calendar));
                    break;
                }
            }
            sender.sendMessage("§3§lAvatarBot §8§l» §7L'UUID §c" + uuid.toString() + " §7a été ajouté à la blacklist.");
            return true;
        } catch (Exception e) {
            sender.sendMessage("§c§lAvatarBot §4§l» §cVous n'avez pas entré d'uuid.");
            return false;
        }
    }
}
