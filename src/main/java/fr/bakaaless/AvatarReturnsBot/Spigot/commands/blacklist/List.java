package fr.bakaaless.AvatarReturnsBot.Spigot.commands.blacklist;

import fr.bakaaless.AvatarReturnsBot.Spigot.mysql.DBConnection;
import fr.bakaaless.AvatarReturnsBot.Spigot.plugin.SpigotMain;
import org.bukkit.command.CommandSender;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

class List {

    static boolean handle(final CommandSender sender, final String[] args) {
        if (!sender.hasPermission("avatarbot.blacklist.list")) {
            sender.sendMessage("§c§lAvatarBot §4§l» §cVous n'avez pas accès à cette commande.");
            return false;
        }
        if (!DBConnection.isOpen()) {
            sender.sendMessage("§c§lAvatarBot §4§l» §cLa base de données n'est pas disponible.");
            return false;
        }
        try (final ResultSet resultSet = DBConnection.sql.readQuery("SELECT * FROM `blacklist`")) {
            boolean b = true;
            final Calendar calendar = Calendar.getInstance();
            sender.sendMessage("§3§lAvatarBot §8§l» §7Liste des personnes présentes sur la blacklist : ");
            while (resultSet.next()) {
                calendar.setTimeInMillis(resultSet.getLong("date"));
                sender.sendMessage("§b-|> §c" + resultSet.getString("uuid") + " §7à §c" + SpigotMain.getString(calendar) + " §7par §c" + resultSet.getString("staff"));
                b = false;
            }
            if (b)
                sender.sendMessage("§cAucune personne n'est sur la blacklist.");
            resultSet.close();
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        sender.sendMessage("§c§lAvatarBot §4§l» §cErreur lors de la requête.");
        return false;
    }
}
