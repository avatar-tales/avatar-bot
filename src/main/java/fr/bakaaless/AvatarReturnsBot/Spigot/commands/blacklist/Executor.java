package fr.bakaaless.AvatarReturnsBot.Spigot.commands.blacklist;

import fr.bakaaless.AvatarReturnsBot.Spigot.plugin.SpigotMain;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

public class Executor implements CommandExecutor {

    private final SpigotMain main;

    public Executor() {
        this.main = SpigotMain.getInstance();
        this.main.getCommand("blacklist").setExecutor(this);
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        try {
            if (args[0].equalsIgnoreCase("add"))
                return Add.handle(sender, args);
            if (args[0].equalsIgnoreCase("list"))
                return List.handle(sender, args);
            if (args[0].equalsIgnoreCase("remove"))
                return Remove.handle(sender, args);
        } catch (IndexOutOfBoundsException ignored) {
            sender.sendMessage("§c§lAvatarBot §4§l» §cArguments incomplets...");
        }
        return true;
    }
}
