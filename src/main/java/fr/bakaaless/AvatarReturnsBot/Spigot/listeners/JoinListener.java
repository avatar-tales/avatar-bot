package fr.bakaaless.AvatarReturnsBot.Spigot.listeners;

import fr.bakaaless.AvatarReturnsBot.Spigot.mysql.DBConnection;
import fr.bakaaless.AvatarReturnsBot.Spigot.plugin.SpigotMain;
import lombok.Getter;
import org.bukkit.event.player.PlayerJoinEvent;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

class JoinListener {

    @Getter
    private final Listening listening;

    JoinListener(final Listening listening) {
        this.listening = listening;
    }

    void handle(final PlayerJoinEvent e) {
        if (DBConnection.isOpen()) {
            boolean kicked = false;
            final Calendar calendar = Calendar.getInstance();
            try (final ResultSet resultSet = DBConnection.sql.readQuery("SELECT * FROM `blacklist` WHERE `uuid` = '" + e.getPlayer().getUniqueId().toString() + "'")) {
                if (resultSet.next()) {
                    calendar.setTimeInMillis(resultSet.getLong("date"));
                    e.getPlayer().kickPlayer("§8§l- §c§lAvatar Security §8§l-\n\n§7Vous ne vous êtes pas enregistré avec\nvotre compte discord pourtant\nvous avez pu entrer.\n\n§c§lDate §8§l» §7" + SpigotMain.getString(calendar));
                    kicked = true;
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            if (kicked)
                return;
        }
        if (this.getListening().getMain().getFileManager().getFile("spigot-features").getBoolean("join.enabled"))
            e.setJoinMessage(this.getListening().getMain().getFileManager().getMessage("spigot-features", "join.message").replace("%player%", e.getPlayer().getName()));
        if (this.listening.getMain().getFileManager().getFile("spigot-features").getBoolean("discordVerify")) {
            this.listening.getMain().getCheckPlayer().accept(e.getPlayer());
        }
    }

}
