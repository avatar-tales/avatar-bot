package fr.bakaaless.AvatarReturnsBot.Spigot.listeners;

import fr.bakaaless.AvatarReturnsBot.Spigot.plugin.SpigotMain;
import lombok.Getter;
import org.bukkit.event.player.PlayerQuitEvent;

class QuitListener {

    @Getter
    private final Listening listening;

    QuitListener(final Listening listening) {
        this.listening = listening;
    }

    void handle(final PlayerQuitEvent e) {
        if (!this.getListening().getMain().getFileManager().getFile("spigot-features").getBoolean("quit.enabled"))
            return;
        e.setQuitMessage(this.getListening().getMain().getFileManager().getMessage("spigot-features", "quit.message").replace("%player%", e.getPlayer().getName()));
        SpigotMain.getInstance().getPlayerBooleanMap().remove(e.getPlayer());
    }
}
