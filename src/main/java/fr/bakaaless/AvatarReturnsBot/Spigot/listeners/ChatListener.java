package fr.bakaaless.AvatarReturnsBot.Spigot.listeners;

import lombok.Getter;
import org.bukkit.event.player.AsyncPlayerChatEvent;

class ChatListener {

    @Getter
    private final Listening listening;

    ChatListener(final Listening listening) {
        this.listening = listening;
    }

    void handle(final AsyncPlayerChatEvent e) {
        if (!this.getListening().getMain().getFileManager().getFile("spigot-features").getBoolean("chat.enabled"))
            return;
        e.setFormat(this.getListening().getMain().getFileManager().getMessage("spigot-features", "chat.message").replace("%player%", e.getPlayer().getDisplayName()).replace("%message%", e.getMessage()));
    }

}
