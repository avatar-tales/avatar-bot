package fr.bakaaless.AvatarReturnsBot.Spigot.listeners;

import com.massivecraft.factions.event.EventFactionsDisband;
import lombok.Getter;
import org.bukkit.entity.Player;

class FactionDisband {

    @Getter
    private final FactionsListener listening;

    FactionDisband(final FactionsListener listening) {
        this.listening = listening;
    }

    void handle(final EventFactionsDisband e) {
        if (e.isCancelled()) return;
        if (e.getSender() instanceof Player) {
            this.getListening().getMain().getOutgoingMessages().deleteFaction((Player) e.getSender(), e.getFaction().getName());
        }
    }
}
