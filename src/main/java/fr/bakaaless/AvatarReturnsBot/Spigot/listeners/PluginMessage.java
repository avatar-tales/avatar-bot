package fr.bakaaless.AvatarReturnsBot.Spigot.listeners;

import fr.bakaaless.AvatarReturnsBot.Spigot.plugin.SpigotMain;
import fr.bakaaless.AvatarReturnsBot.Spigot.utils.MCVersion;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.ArrayList;

public class PluginMessage implements PluginMessageListener {

    @Getter
    private final SpigotMain main;

    @Getter
    private final String channel;

    public PluginMessage(final SpigotMain main) {
        this.main = main;
        if (MCVersion.get().isInferior(MCVersion.v1_13)) {
            this.channel = "BungeeBot";
        } else {
            this.channel = "bungee:bot";
        }
        this.getMain().getServer().getMessenger().registerIncomingPluginChannel(this.getMain(), this.getChannel(), this);
    }

    @Override
    public void onPluginMessageReceived(@NotNull String channel, @NotNull Player player, @NotNull byte[] message) {
        if (!channel.equals(this.getChannel())) return;
        String action = null;
        final ArrayList<String> received = new ArrayList<>();
        final DataInputStream in = new DataInputStream(new ByteArrayInputStream(message));
        try {
            action = in.readUTF();
            while (in.available() > 0) {
                received.add(in.readUTF());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (action == null) return;
        if (action.equals("Reload")) {
            this.getMain().getFileManager().reload();
            return;
        }
        if (action.equals("responseRegistered")) {
            final Player p = this.main.getServer().getPlayer(received.get(0));
            final boolean response = received.get(1).equalsIgnoreCase("true");
            SpigotMain.updateDiscord(p, response);

        }
    }

}
