package fr.bakaaless.AvatarReturnsBot.Spigot.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * @author BakaAless ont 7/05/2020
 */
class PlayerCommandPreprocess {

    private final Listening listening;

    PlayerCommandPreprocess(final Listening listening) {
        this.listening = listening;
    }

    /**
     * @param e the event
     */
    void handle(final PlayerCommandPreprocessEvent e) {
        if (this.listening.getMain().getFileManager().getFile("spigot-features").getBoolean("discordVerify")) {
            if (!this.listening.getMain().getPlayerBooleanMap().containsKey(e.getPlayer()))
                Bukkit.broadcastMessage("cancelled #1");
                //e.setCancelled(true);
            else {
                if (!this.listening.getMain().getPlayerBooleanMap().get(e.getPlayer()))
                    Bukkit.broadcastMessage("cancelled #2");
            }
        }
    }
}
