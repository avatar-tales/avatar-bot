package fr.bakaaless.AvatarReturnsBot.Spigot.listeners;

import fr.bakaaless.AvatarReturnsBot.Spigot.plugin.SpigotMain;
import lombok.Getter;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class Listening implements Listener {

    @Getter
    private final SpigotMain main;

    public Listening(final SpigotMain main) {
        this.main = main;
        this.getMain().getServer().getPluginManager().registerEvents(this, this.getMain());
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent e) {
        new JoinListener(this).handle(e);
    }

    @EventHandler
    public void onPlayerQuit(final PlayerQuitEvent e) {
        new QuitListener(this).handle(e);
    }

    @EventHandler
    public void onAsyncPlayerChat(final AsyncPlayerChatEvent e) {
        new ChatListener(this).handle(e);
    }

    @EventHandler
    public void onPlayerCommandPreprocess(final PlayerCommandPreprocessEvent e) {
        new PlayerCommandPreprocess(this).handle(e);
    }

}
