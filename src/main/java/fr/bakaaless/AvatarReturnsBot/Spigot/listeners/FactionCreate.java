package fr.bakaaless.AvatarReturnsBot.Spigot.listeners;

import com.massivecraft.factions.event.EventFactionsCreate;
import lombok.Getter;
import org.bukkit.entity.Player;

class FactionCreate {

    @Getter
    private final FactionsListener listening;

    FactionCreate(final FactionsListener listening) {
        this.listening = listening;
    }

    void handle(final EventFactionsCreate e) {
        if (e.isCancelled()) return;
        if (e.getSender() instanceof Player) {
            this.getListening().getMain().getOutgoingMessages().createFaction((Player) e.getSender(), e.getFactionName());
        }
    }
}
