package fr.bakaaless.AvatarReturnsBot.Spigot.listeners;

import com.massivecraft.factions.event.EventFactionsNameChange;
import lombok.Getter;
import org.bukkit.entity.Player;

class FactionChangeName {

    @Getter
    private final FactionsListener listening;

    FactionChangeName(final FactionsListener listening) {
        this.listening = listening;
    }

    void handle(final EventFactionsNameChange e) {
        if (e.isCancelled()) return;
        if (e.getSender() instanceof Player) {
            this.getListening().getMain().getOutgoingMessages().changeName((Player) e.getSender(), e.getFaction().getName(), e.getNewName());
        }
    }
}
