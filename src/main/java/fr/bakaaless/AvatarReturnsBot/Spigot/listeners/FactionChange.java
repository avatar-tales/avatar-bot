package fr.bakaaless.AvatarReturnsBot.Spigot.listeners;

import com.massivecraft.factions.event.EventFactionsMembershipChange;
import lombok.Getter;
import org.bukkit.entity.Player;

class FactionChange {

    @Getter
    private final FactionsListener listening;

    FactionChange(final FactionsListener listening) {
        this.listening = listening;
    }

    void handle(final EventFactionsMembershipChange e) {
        if (e.isCancelled()) return;
        if (e.getReason().equals(EventFactionsMembershipChange.MembershipChangeReason.JOIN)) {
            this.getListening().getMain().getOutgoingMessages().joinFaction((Player) e.getSender(), e.getNewFaction().getName());
        } else if (e.getReason().equals(EventFactionsMembershipChange.MembershipChangeReason.LEAVE)) {
            this.getListening().getMain().getOutgoingMessages().leaveFaction((Player) e.getSender(), e.getMPlayer().getFaction().getName());
        } else if (e.getReason().equals(EventFactionsMembershipChange.MembershipChangeReason.KICK)) {
            this.getListening().getMain().getOutgoingMessages().kickFaction((Player) e.getSender(), e.getMPlayer().getName(), e.getMPlayer().getFaction().getName());
        }
    }
}
