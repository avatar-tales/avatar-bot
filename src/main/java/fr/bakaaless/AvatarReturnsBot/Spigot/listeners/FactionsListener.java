package fr.bakaaless.AvatarReturnsBot.Spigot.listeners;

import com.massivecraft.factions.event.EventFactionsCreate;
import com.massivecraft.factions.event.EventFactionsDisband;
import com.massivecraft.factions.event.EventFactionsMembershipChange;
import com.massivecraft.factions.event.EventFactionsNameChange;
import fr.bakaaless.AvatarReturnsBot.Spigot.plugin.SpigotMain;
import lombok.Getter;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class FactionsListener implements Listener {

    @Getter
    private final SpigotMain main;

    public FactionsListener(SpigotMain main) {
        this.main = main;
        this.getMain().getServer().getPluginManager().registerEvents(this, this.getMain());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onFactionsCreate(final EventFactionsCreate e) {
        new FactionCreate(this).handle(e);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onFactionsChangeName(final EventFactionsNameChange e) {
        new FactionChangeName(this).handle(e);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onFactionsDisband(final EventFactionsDisband e) {
        new FactionDisband(this).handle(e);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onFactionsMembershipChange(final EventFactionsMembershipChange e) {
        new FactionChange(this).handle(e);
    }

}
