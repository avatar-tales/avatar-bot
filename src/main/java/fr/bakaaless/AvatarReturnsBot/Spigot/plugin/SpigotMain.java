package fr.bakaaless.AvatarReturnsBot.Spigot.plugin;

import fr.bakaaless.AvatarReturnsBot.Spigot.commands.blacklist.Executor;
import fr.bakaaless.AvatarReturnsBot.Spigot.listeners.FactionsListener;
import fr.bakaaless.AvatarReturnsBot.Spigot.listeners.Listening;
import fr.bakaaless.AvatarReturnsBot.Spigot.listeners.PluginMessage;
import fr.bakaaless.AvatarReturnsBot.Spigot.messages.OutgoingMessages;
import fr.bakaaless.AvatarReturnsBot.Spigot.mysql.DBConnection;
import fr.bakaaless.AvatarReturnsBot.Spigot.utils.FileManager;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;
import java.util.function.Consumer;

public class SpigotMain extends JavaPlugin {

    private static SpigotMain instance;

    @Getter
    private FileManager fileManager;

    @Getter
    private Listening listening;

    @Getter
    private PluginMessage pluginMessage;

    @Getter
    private OutgoingMessages outgoingMessages;

    @Getter
    private Executor executor;

    @Getter
    private Optional<FactionsListener> factionsListenerOptional;

    @Getter
    private Map<Player, Boolean> playerBooleanMap;

    @Getter
    private Consumer<Player> checkPlayer;

    public static SpigotMain getInstance() {
        return SpigotMain.instance;
    }

    public static void updateDiscord(Player p, boolean response) {
        final SpigotMain plugin = SpigotMain.getInstance();
        if (plugin.getPlayerBooleanMap().putIfAbsent(p, response) == null) {
            plugin.getPlayerBooleanMap().remove(p);
            plugin.getPlayerBooleanMap().put(p, true);
        }
        if (!response) {
            if (DBConnection.isOpen()) {
                final Calendar calendar = Calendar.getInstance();
                DBConnection.sql.modifyQuery("INSERT INTO `blacklist` (`uuid`,`date`, `staff`) VALUES ('" + p.getUniqueId().toString() + "'," + calendar.getTimeInMillis() + ", 'CONSOLE')", true);
                p.kickPlayer("§8§l- §c§lAvatar Security §8§l-\n\n§7Vous ne vous êtes pas enregistré avec\nvotre compte discord pourtant\nvous avez pu entrer.\n\n§c§lDate §8§l» §7" + SpigotMain.getString(calendar));
            }
        }
    }

    public static String getString(final Calendar calendar) {
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);
        int seconds = calendar.get(Calendar.SECOND);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        return (hour < 10 ? "0" : "") + hour + ":" +
                (minutes < 10 ? "0" : "") + minutes + ":" +
                (seconds < 10 ? "0" : "") + seconds + " " +
                (day < 10 ? "0" : "") + day + "/" +
                (month < 10 ? "0" : "") + month + "/" +
                year;
    }

    public static Timestamp convert(Date date) {
        return new Timestamp(date.getTime());
    }

    @Override
    public void onEnable() {
        SpigotMain.instance = this;
        this.fileManager = new FileManager(this, "spigot-features", "database");
        DBConnection.init();
        this.listening = new Listening(this);
        this.pluginMessage = new PluginMessage(this);
        this.outgoingMessages = new OutgoingMessages(this);
        this.executor = new Executor();
        this.factionsListenerOptional = Optional.of(new FactionsListener(this));
        this.playerBooleanMap = new HashMap<>();
        this.checkPlayer = player -> {
            this.getServer().getScheduler().runTaskLater(this, () -> {
                this.getOutgoingMessages().verifyDiscordUser(player);
            }, 10L);
            this.getServer().getScheduler().runTaskLater(this, () -> {


                if (!this.getPlayerBooleanMap().containsKey(player)) {
                    if (DBConnection.sql.tableExists("players_info")) {
                        try (final ResultSet resultSet = DBConnection.sql.readQuery("SELECT * FROM `players_info` WHERE `name` = '" + player.getName() + "'")) {
                            if (resultSet.next()) {
                                if (!resultSet.getString("discordId").equals("") && !resultSet.getString("discordId").equalsIgnoreCase(" ")) {
                                    if (resultSet.getString("online").equals("1")) {
                                        SpigotMain.updateDiscord(player, true);
                                        resultSet.close();
                                        return;
                                    }
                                }
                            }
                        } catch (SQLException ignored) {

                        }
                    }
                    SpigotMain.updateDiscord(player, false);
                }
            }, 30 * 20L);
        };
        for (final Player player : this.getServer().getOnlinePlayers()) {
            checkPlayer.accept(player);
        }
    }

    @Override
    public void onDisable() {
        if (DBConnection.isOpen()) {
            DBConnection.sql.close();
        }
    }
}
