package fr.bakaaless.AvatarReturnsBot.Spigot.utils;

import fr.bakaaless.AvatarReturnsBot.Spigot.plugin.SpigotMain;
import org.bukkit.ChatColor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class FileManager {

    private final SpigotMain main;
    private final Map<String, File> filesMap;
    private final Map<String, YamlConfiguration> yamlMap;

    public FileManager(final SpigotMain main, final String... files) {
        this.main = main;
        this.filesMap = new HashMap<>();
        this.yamlMap = new HashMap<>();
        this.init(files);
    }

    public void init(final String... files) {
        for (String file : files) {
            this.filesMap.put(file, new File(main.getDataFolder(), file + ".yml"));
            if (!this.filesMap.get(file).exists()) {
                this.filesMap.get(file).getParentFile().mkdirs();
                main.saveResource(file + ".yml", false);
            }
            this.yamlMap.put(file, new YamlConfiguration());
            try {
                this.yamlMap.get(file).load(this.filesMap.get(file));
            } catch (IOException | InvalidConfigurationException e) {
                e.printStackTrace();
            }
        }
    }

    public void reload() {
        for (final String file : this.filesMap.keySet()) {
            this.filesMap.replace(file, new File(this.main.getDataFolder(), file + ".yml"));
            if (!this.filesMap.get(file).exists()) {
                this.filesMap.get(file).getParentFile().mkdirs();
                main.saveResource(file + ".yml", false);
            }
            this.yamlMap.replace(file, new YamlConfiguration());
            try {
                this.yamlMap.get(file).load(this.filesMap.get(file));
            } catch (IOException | InvalidConfigurationException e) {
                e.printStackTrace();
            }
        }
    }

    public void setLine(final String file, final String path, final Object value) {
        this.yamlMap.get(file).set(path, value);
        try {
            this.yamlMap.get(file).save(this.filesMap.get(file));
            this.yamlMap.get(file).load(this.filesMap.get(file));
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public FileConfiguration getFile(final String file) {
        return this.yamlMap.get(file);
    }

    public String getMessage(final String file, final String path) {
        if (this.yamlMap.get(file).getString(path) == null) return "";
        return ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(this.yamlMap.get(file).getString(path)));
    }

}
