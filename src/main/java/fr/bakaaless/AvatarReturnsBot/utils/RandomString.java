package fr.bakaaless.AvatarReturnsBot.utils;

import lombok.Getter;

import java.util.Random;

public class RandomString {

    @Getter
    private final int length;

    public RandomString(final int length) {
        this.length = length;
    }

    public String getString() {
        String[] dataSet = {"&", "#", "@", "$", "/", "\\",
                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
                "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
                "u", "v", "w", "x", "y", "z", "A", "B", "C", "D",
                "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
                "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X",
                "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7",
                "8", "9"};
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < length; i++)
            result.append(dataSet[new Random().nextInt(dataSet.length)]);
        return result.toString();
    }

    public String getString(String[] dataSet) {
        final StringBuilder result = new StringBuilder();
        for (int i = 0; i < length; i++)
            result.append(dataSet[new Random().nextInt(dataSet.length)]);
        return result.toString();
    }
}
