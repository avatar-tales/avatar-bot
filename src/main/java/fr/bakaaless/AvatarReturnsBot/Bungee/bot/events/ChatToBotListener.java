package fr.bakaaless.AvatarReturnsBot.Bungee.bot.events;

import fr.bakaaless.AvatarReturnsBot.Bungee.bot.BotDiscord;
import fr.bakaaless.AvatarReturnsBot.Bungee.mysql.DBConnection;
import fr.bakaaless.AvatarReturnsBot.utils.RandomString;
import lombok.Getter;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;
import net.dv8tion.jda.internal.entities.UserImpl;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;

public class ChatToBotListener implements Listener {

    @Getter
    private final BotDiscord botDiscord;

    @Getter
    private final Map<ProxiedPlayer, String> codes;

    @Getter
    private final Map<ProxiedPlayer, Member> discordID;

    public ChatToBotListener(final BotDiscord botDiscord) {
        this.botDiscord = botDiscord;
        this.codes = new HashMap<>();
        this.discordID = new HashMap<>();
        this.getBotDiscord().getMain().getProxy().getPluginManager().registerListener(this.getBotDiscord().getMain(), this);
    }

    @EventHandler
    public void onChat(final ChatEvent e) {
        final ProxiedPlayer pp = (ProxiedPlayer) e.getSender();
        if (pp.getServer().getInfo().getName().equalsIgnoreCase(this.getBotDiscord().getMain().getFileManager().getFile("config").getString("discord-redirection.waiting-server"))) {
            final String discordId = this.botDiscord.getMain().getDiscordIds().get(pp);
            if (discordId.equals(" ") || discordId.equals("") || !this.getBotDiscord().getMain().getBotDiscord().getServerContainsPlayerID(discordId)) {
                e.setCancelled(true);
                if (!this.getCodes().containsKey(pp)) {
                    final String[] dataSet = {"0", "1", "2", "3", "4", "5", "6", "7",
                            "8", "9"};
                    final String code = new RandomString(7).getString(dataSet);
                    Thread thread = new Thread(() -> this.sendMessageCode(pp, code, e.getMessage()));
                    thread.setDaemon(true);
                    thread.start();
                    return;
                } else {
                    if (this.getCodes().get(pp).equalsIgnoreCase(e.getMessage())) {
                        DBConnection.sql.modifyQuery("UPDATE `players_info` SET `discordId` = '" + this.getDiscordID().get(pp).getUser().getId() + "' WHERE `uuid` = '" + pp.getUniqueId() + "'", false);
                        this.botDiscord.getMain().getDiscordIds().replace(pp, this.getDiscordID().get(pp).getUser().getId());
                        this.getCodes().remove(pp);
                        if (this.getBotDiscord().getMain().getFileManager().getFile("config").getBoolean("discord-roles.enabled")) {
                            final Optional<Guild> guild = Optional.ofNullable(this.getBotDiscord().getJda().getGuildById(this.getBotDiscord().getMain().getFileManager().getFile("config").getString("discord-roles.serverID")));
                            if (!guild.isPresent()) return;
                            final Optional<Role> roleOptional = Optional.ofNullable(guild.get().getRoleById(this.getBotDiscord().getMain().getFileManager().getFile("config").getString("discord-roles.registeredRoleID")));
                            if (!roleOptional.isPresent()) return;
                            guild.get().addRoleToMember(this.getDiscordID().get(pp), roleOptional.get()).complete();
                            final Optional<Role> oldRoleOptional = Optional.ofNullable(guild.get().getRoleById(this.getBotDiscord().getMain().getFileManager().getFile("config").getString("discord-roles.joinRoleID")));
                            if (!oldRoleOptional.isPresent()) return;
                            guild.get().removeRoleFromMember(this.getDiscordID().get(pp), oldRoleOptional.get()).complete();
                        }
                        this.getDiscordID().remove(pp);
                        pp.sendMessage(TextComponent.fromLegacyText(this.getBotDiscord().getMain().getFileManager().getMessage("link.message")));
                        pp.connect(this.getBotDiscord().getMain().getProxy().getServerInfo(this.getBotDiscord().getMain().getFileManager().getFile("config").getString("discord-redirection.redirection-server")));
                        return;
                    } else if (!e.getMessage().equalsIgnoreCase("stop")) {
                        pp.sendMessage(TextComponent.fromLegacyText(this.getBotDiscord().getMain().getFileManager().getError("link.invalid.code")));
                        return;
                    } else {
                        this.getCodes().remove(pp);
                        this.getDiscordID().remove(pp);
                        pp.sendMessage(TextComponent.fromLegacyText(this.getBotDiscord().getMain().getFileManager().getMessage("link.accounts")));
                        return;
                    }
                }
            }
        }
        Thread thread = new Thread(() -> {
            try {
                sendMessage(e);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    public void sendMessage(final ChatEvent e) throws InterruptedException {
        Thread.sleep(500);
        if (e.isCancelled()) return;
        if (!(e.getSender() instanceof ProxiedPlayer)) return;
        final ProxiedPlayer pp = (ProxiedPlayer) e.getSender();
        if (e.getMessage().startsWith("/")) return;
        if (!e.getMessage().startsWith("!")) return;
        if (e.getMessage().replaceFirst("!", "").equals("") || e.getMessage().replaceFirst("!", "").equals(" ")) return;
        for (final String servers : this.getBotDiscord().getMain().getFileManager().getFile("config").getSection("betweenworlds").getKeys()) {
            if (!servers.toLowerCase().equals(pp.getServer().getInfo().getName())) continue;
            if (this.getBotDiscord().getMain().getFileManager().getFile("config").getBoolean("betweenworlds." + servers + ".enabled")) {
                String message = this.getBotDiscord().getMain().getFileManager().getFile("config").getString("betweenworlds." + servers + ".format");
                message = message.replace("%user%", pp.getDisplayName());
                message = message.replace("%message%", e.getMessage().replaceFirst("!", ""));
                for (int i = 0; i < 10; i++)
                    message = message.replace("&" + i, "");
                message = message.replace("&a", "")
                        .replace("&b", "")
                        .replace("&c", "")
                        .replace("&d", "")
                        .replace("&e", "")
                        .replace("&f", "")
                        .replace("&k", "")
                        .replace("&l", "")
                        .replace("&m", "")
                        .replace("&o", "")
                        .replace("&n", "")
                        .replace("&r", "")
                        .replace("_", "\\_")
                        .replace("`", "\\`")
                        .replace("*", "\\*")
                        .replace("here", "ceuxQuiSontLà")
                        .replace("everyone", "toutLeMonde");
                GuildChannel guildChannel = this.getBotDiscord().getJda().getGuildChannelById(this.getBotDiscord().getMain().getFileManager().getFile("config").getString("betweenworlds." + servers + ".channelID"));
                if (guildChannel == null) return;
                if (!(guildChannel instanceof TextChannel)) return;
                ((TextChannel) guildChannel).sendMessage(message).complete();
            }
        }
    }

    public void sendMessageCode(final ProxiedPlayer pp, final String code, final String tag) {
        ProxyServer.getInstance().getLogger().log(Level.INFO, "SendCode :" + pp + " : " + code + " : " + tag);
        final Guild guild = this.getBotDiscord().getJda().getGuildById(this.getBotDiscord().getMain().getFileManager().getFile("config").getString("discord-redirection.serverID"));
        ProxyServer.getInstance().getLogger().log(Level.INFO, "Checking guild...");
        if (guild == null) return;
        ProxyServer.getInstance().getLogger().log(Level.INFO, "found.");
        for (final Member member : guild.getMembers()) {
            if (tag.toLowerCase().equalsIgnoreCase(member.getUser().getName().toLowerCase() + "#" + member.getUser().getDiscriminator().toLowerCase())) {
                try {
                    ProxyServer.getInstance().getLogger().log(Level.INFO, "Start trying...");
                    ProxyServer.getInstance().getLogger().log(Level.INFO, "Checking already linked...");
                    if (this.getBotDiscord().getMain().isDiscordIDAlreadyLinked(member.getUser().getId())) {
                        System.err.println(this.getBotDiscord().getMain().getFileManager().getError("link.already"));
                        pp.sendMessage(TextComponent.fromLegacyText(this.getBotDiscord().getMain().getFileManager().getError("link.already")));
                        return;
                    }
                    ProxyServer.getInstance().getLogger().log(Level.INFO, "Checking opening private channel...");
                    if (!member.getUser().hasPrivateChannel())
                        member.getUser().openPrivateChannel().complete();
                    ProxyServer.getInstance().getLogger().log(Level.INFO, "Sending messages...");
                    ((UserImpl) member.getUser()).getPrivateChannel().sendMessage(this.getBotDiscord().getMain().getFileManager().getMessage("link.code-mp").replace("%player%", pp.getDisplayName()).replace("%code%", code)).complete();
                    ((UserImpl) member.getUser()).getPrivateChannel().close().complete();
                    pp.sendMessage(TextComponent.fromLegacyText(this.getBotDiscord().getMain().getFileManager().getMessage("link.see-mp")));
                    this.getCodes().put(pp, code);
                    this.getDiscordID().put(pp, member);
                    ProxyServer.getInstance().getLogger().log(Level.INFO, "End.");
                } catch (ErrorResponseException ignored) {
                    pp.sendMessage(TextComponent.fromLegacyText(this.getBotDiscord().getMain().getFileManager().getError("link.permission")));
                }
                return;
            }
        }
        ProxyServer.getInstance().getLogger().log(Level.INFO, "No member found...");
        pp.sendMessage(TextComponent.fromLegacyText(this.getBotDiscord().getMain().getFileManager().getError("link.invalid.tag").replace("%tag%", tag)));

    }
}
