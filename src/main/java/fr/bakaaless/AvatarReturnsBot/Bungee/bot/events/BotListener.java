package fr.bakaaless.AvatarReturnsBot.Bungee.bot.events;

import fr.bakaaless.AvatarReturnsBot.Bungee.bot.BotDiscord;
import fr.bakaaless.AvatarReturnsBot.Bungee.mysql.DBConnection;
import lombok.Getter;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRemoveEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import javax.annotation.Nonnull;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class BotListener extends ListenerAdapter {

    @Getter
    private final BotDiscord botDiscord;

    public BotListener(final BotDiscord botDiscord) {
        this.botDiscord = botDiscord;
    }

    @Override
    public void onGuildMemberJoin(@Nonnull GuildMemberJoinEvent event) {
        if (!this.getBotDiscord().getMain().getFileManager().getFile("config").getBoolean("discord-roles.enabled"))
            return;
        if (!event.getGuild().getId().equalsIgnoreCase(this.getBotDiscord().getMain().getFileManager().getFile("config").getString("discord-roles.serverID")))
            return;
        final Optional<Role> roleOptional = Optional.ofNullable(event.getGuild().getRoleById(this.getBotDiscord().getMain().getFileManager().getFile("config").getString("discord-roles.joinRoleID")));
        if (!roleOptional.isPresent()) return;
        event.getGuild().addRoleToMember(event.getMember(), roleOptional.get()).complete();
    }

    @Override
    public void onGuildMemberRemove(@Nonnull GuildMemberRemoveEvent e) {
        try (final ResultSet resultSet = DBConnection.sql.readQuery("SELECT * FROM `players_info` WHERE `discordId` = '" + e.getUser().getId() + "'")) {
            if (resultSet.next()) {
                DBConnection.sql.modifyQuery("UPDATE `players_info` SET `discordId` = '' WHERE `discordId` = '" + e.getUser().getId() + "'");
            }
        } catch (SQLException ignored) {
        }
    }

    @Override
    public void onMessageReceived(@Nonnull final MessageReceivedEvent e) {
        if (e.getAuthor().equals(this.getBotDiscord().getJda().getSelfUser())) return;
        if (e.getMessage().getContentRaw().equals("")) return;
        if (this.getBotDiscord().getMain().getFileManager().getFile("config").getSection("betweenworlds") == null)
            return;
        for (final String servers : this.getBotDiscord().getMain().getFileManager().getFile("config").getSection("betweenworlds").getKeys()) {
            if (this.getBotDiscord().getMain().getFileManager().getFile("config").getBoolean("betweenworlds." + servers + ".enabled")) {
                if (this.getBotDiscord().getMain().getFileManager().getFile("config").getString("betweenworlds." + servers + ".channelID").equals(e.getTextChannel().getId())) {
                    String message = this.getBotDiscord().getMain().getFileManager().getFile("config").getString("betweenworlds." + servers + ".format");
                    String name = e.getAuthor().getName();
                    try (final ResultSet resultSet = DBConnection.sql.readQuery("SELECT * FROM `players_info` WHERE `discordId` = '" + e.getAuthor().getId() + "'")) {
                        if (resultSet.next()) {
                            name = resultSet.getString("name");
                        }
                    } catch (SQLException ignored) {

                    }
                    message = message.replace("%user%", name);
                    message = message.replace("%message%", e.getMessage().getContentRaw());
                    for (final ProxiedPlayer pp : this.getBotDiscord().getMain().getProxy().getPlayers()) {
                        if (pp == null) return;
                        if (pp.getServer() == null) return;
                        if (pp.getServer().getInfo().getName().toLowerCase().equals(servers.toLowerCase())) {
                            pp.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', message)));
                        }
                    }
                }
            }
        }
    }
}
