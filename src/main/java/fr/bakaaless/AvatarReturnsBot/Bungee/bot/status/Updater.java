package fr.bakaaless.AvatarReturnsBot.Bungee.bot.status;

import java.util.Timer;
import java.util.TimerTask;

public class Updater {

    public Updater() {
        final Timer timerTask = new Timer();
        timerTask.schedule(new TimerTask() {
            @Override
            public void run() {
                Updater.update();
            }
        }, 5 * 60 * 1000);
    }

    public static void update() {

    }
}
