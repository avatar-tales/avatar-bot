package fr.bakaaless.AvatarReturnsBot.Bungee.bot;

import fr.bakaaless.AvatarReturnsBot.Bungee.bot.events.BotListener;
import fr.bakaaless.AvatarReturnsBot.Bungee.bot.events.ChatToBotListener;
import fr.bakaaless.AvatarReturnsBot.Bungee.plugin.BungeeMain;
import lombok.Getter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.ChunkingFilter;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

import javax.security.auth.login.LoginException;
import java.util.logging.Level;

//import net.dv8tion.jda.api.requests.GatewayIntent;

public class BotDiscord {

    @Getter
    private final BungeeMain main;

    @Getter
    private JDA jda;

    @Getter
    private boolean started;

    public BotDiscord(final BungeeMain main) {
        this.main = main;
        this.started = false;
        try {
            this.jda = JDABuilder
                    .createDefault(
                            this.getMain().getFileManager().getFile("config").getString("token"),
                            GatewayIntent.GUILD_MESSAGES,
                            GatewayIntent.GUILD_MEMBERS,
                            GatewayIntent.GUILD_PRESENCES
                    )
                    .disableCache(
                            CacheFlag.VOICE_STATE,
                            CacheFlag.EMOTE
                    )
                    .setStatus(OnlineStatus.IDLE)
                    .setChunkingFilter(ChunkingFilter.NONE)
                    .setMemberCachePolicy(MemberCachePolicy.ALL)
                    .addEventListeners(new BotListener(this))
                    .build();
            new ChatToBotListener(this);
            this.started = true;
        } catch (LoginException e) {
            this.getMain().getProxy().getLogger().log(Level.SEVERE, this.getMain().getFileManager().getError("bot-creation"), e);
        }
    }

    public boolean getServerContainsPlayerID(final String id) {
        final Guild guild = this.getJda().getGuildById(this.getMain().getFileManager().getFile("config").getString("discord-redirection.serverID"));
        if (guild == null) return true;
        for (final Member member : guild.getMembers()) {
            if (member.getUser().getId().equals(id)) return true;
        }
        return false;
    }
}
