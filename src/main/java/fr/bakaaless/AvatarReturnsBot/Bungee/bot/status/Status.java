package fr.bakaaless.AvatarReturnsBot.Bungee.bot.status;

import fr.bakaaless.AvatarReturnsBot.Bungee.bot.BotDiscord;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.List;

public class Status extends ListenerAdapter {

    // TODO : use a database such as MySQL to save and get servers' information
    private final BotDiscord botDiscord;
    private final Message message;

    public Status(final BotDiscord botDiscord, final String channelId) {
        this.botDiscord = botDiscord;
        GuildChannel guildChannel = this.botDiscord.getJda().getGuildChannelById(channelId);
        if (guildChannel instanceof TextChannel) {
            MessageHistory history = new MessageHistory((MessageChannel) guildChannel);
            List<Message> messages;
            try {
                while (true) {
                    messages = history.retrievePast(1).complete();
                    if (!messages.get(0).isPinned()) messages.get(0).delete().complete();
                }
            } catch (Exception ignored) {
            }
            this.message = ((TextChannel) guildChannel).sendMessage(new EmbedBuilder().build()).complete();
            this.botDiscord.getJda().addEventListener(this);
            new Updater();
            return;
        }
        this.message = null;
    }
}
