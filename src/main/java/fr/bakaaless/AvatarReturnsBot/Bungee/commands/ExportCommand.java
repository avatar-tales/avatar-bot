package fr.bakaaless.AvatarReturnsBot.Bungee.commands;

import fr.bakaaless.AvatarReturnsBot.Bungee.mysql.DBConnection;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.UUID;

class ExportCommand {

    @Getter
    private final CommandExecutor commandExecutor;

    ExportCommand(final CommandExecutor commandExecutor) {
        this.commandExecutor = commandExecutor;
    }

    /**
     * This method will be used to export player's data
     * from a database (SQLite, MySQL) to a flat file.
     *
     * @param commandSender the {@link CommandSender}
     */
    void handle(final CommandSender commandSender) {
        if (!commandSender.hasPermission("bungeebot.export")) {
            commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "&c&lAvatarBot &4&l» &cVous ne pouvez pas faire cela.")));
            return;
        }
        if (!DBConnection.isOpen()) {
            commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "&c&lAvatarBot &4&l» &cIl n'y a aucune base de données.")));
            return;
        }
        final File file = new File(this.commandExecutor.getMain().getDataFolder(), "export.yml");
        try {
            if (file.exists()) file.delete();
            file.createNewFile();
        } catch (IOException ignored) {
            commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "§c§lAvatarBot §4§l» §cErreur lors de la création du fichier.")));
            return;
        }
        commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "&3&lAvatarBot &8&l» &7L'exportation va se faire de façon asynchrone.")));
        this.commandExecutor.getMain().getProxy().getScheduler().runAsync(this.commandExecutor.getMain(), () -> {
            try {
                final long time = Calendar.getInstance().getTimeInMillis();

                int success = 0;
                int errors = 0;
                try (final ResultSet resultSet = DBConnection.sql.readQuery("SELECT * FROM `players_info`")) {
                    final Configuration configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
                    while (resultSet.next()) {
                        UUID uuid;
                        try {
                            uuid = UUID.fromString(resultSet.getString("uuid"));
                        } catch (Exception ignored) {
                            errors++;
                            continue;
                        }
                        final String name = resultSet.getString("name");
                        final String discordId = resultSet.getString("discordId");
                        try {
                            configuration.set("players." + uuid.toString() + ".name", name);
                            configuration.set("players." + uuid.toString() + ".discordID", discordId);
                            ConfigurationProvider.getProvider(YamlConfiguration.class).save(configuration, new File(this.commandExecutor.getMain().getDataFolder(), "export.yml"));
                            success++;
                        } catch (Exception ignored) {
                            errors++;
                        }
                    }
                } catch (IOException ignored) {
                    commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "§c§lAvatarBot §4§l» §cErreur lors de l'écriture dans le fichier.")));
                    return;
                }
                final long now = Calendar.getInstance().getTimeInMillis();
                commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "&3&lAvatarBot &8&l» &7Résultat de l'exportation :")));
                commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', " &b-|> &a" + success + " &7personne(s) exportée(s),")));
                commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', " &b-|> &c" + errors + " &7erreurs,")));
                commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', " &b-|> &7Durée : &d" + (now - time) + "ms&7.")));
            } catch (SQLException ignored) {
                commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "§c§lAvatarBot §4§l» §cErreur lors de la communication avec la base de données.")));
            }
        });
    }
}
