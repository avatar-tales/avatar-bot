package fr.bakaaless.AvatarReturnsBot.Bungee.commands;

import lombok.Getter;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;

import java.util.List;

class ReloadCommand {

    @Getter
    private final CommandExecutor commandExecutor;

    ReloadCommand(final CommandExecutor commandExecutor) {
        this.commandExecutor = commandExecutor;
    }

    void handle(final CommandSender commandSender, final List<String> args) {
        if (!commandSender.hasPermission("bungeebot.reload")) {
            commandSender.sendMessage(TextComponent.fromLegacyText(this.getCommandExecutor().getMain().getFileManager().getError("permission")));
            return;
        }
        if (args.size() == 0) {
            reload(commandSender);
            return;
        }
        if (args.get(0).equalsIgnoreCase("all")) {
            reload(commandSender);
        }
        if (args.get(0).toLowerCase().startsWith("bungee") || args.get(0).equalsIgnoreCase("proxy")) {
            commandSender.sendMessage(TextComponent.fromLegacyText(this.getCommandExecutor().getMain().getFileManager().getMessage("reload.server").replace("%server%", "Proxy")));
            this.getCommandExecutor().getMain().getFileManager().reload();
            commandSender.sendMessage(TextComponent.fromLegacyText(this.getCommandExecutor().getMain().getFileManager().getMessage("reload.finish")));
            return;
        }
        if (args.get(0).equalsIgnoreCase("servers") || args.get(0).equalsIgnoreCase("serveurs")) {
            commandSender.sendMessage(TextComponent.fromLegacyText(this.getCommandExecutor().getMain().getFileManager().getMessage("reload.servers")));
            for (ServerInfo serverInfo : this.getCommandExecutor().getMain().getProxy().getServers().values())
                this.getCommandExecutor().getMain().getBungeeSendMessage().reload(serverInfo);
            commandSender.sendMessage(TextComponent.fromLegacyText(this.getCommandExecutor().getMain().getFileManager().getMessage("reload.finish")));
            return;
        }
        for (final String server : this.getCommandExecutor().getMain().getProxy().getServers().keySet()) {
            if (server.equalsIgnoreCase(args.get(0))) {
                commandSender.sendMessage(TextComponent.fromLegacyText(this.getCommandExecutor().getMain().getFileManager().getMessage("reload.server").replace("%server%", server)));
                this.getCommandExecutor().getMain().getBungeeSendMessage().reload(server);
                commandSender.sendMessage(TextComponent.fromLegacyText(this.getCommandExecutor().getMain().getFileManager().getMessage("reload.finish")));
                return;
            }
        }

        commandSender.sendMessage(TextComponent.fromLegacyText(this.getCommandExecutor().getMain().getFileManager().getError("reload")));

    }

    private void reload(CommandSender commandSender) {
        commandSender.sendMessage(TextComponent.fromLegacyText(this.getCommandExecutor().getMain().getFileManager().getMessage("reload.all")));
        for (ServerInfo serverInfo : this.getCommandExecutor().getMain().getProxy().getServers().values())
            this.getCommandExecutor().getMain().getBungeeSendMessage().reload(serverInfo);
        this.getCommandExecutor().getMain().getFileManager().reload();
        commandSender.sendMessage(TextComponent.fromLegacyText(this.getCommandExecutor().getMain().getFileManager().getMessage("reload.finish")));
    }

}
