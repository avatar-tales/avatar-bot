package fr.bakaaless.AvatarReturnsBot.Bungee.commands;

import fr.bakaaless.AvatarReturnsBot.Bungee.mysql.DBConnection;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

class ImportCommand {

    @Getter
    private final CommandExecutor commandExecutor;

    ImportCommand(final CommandExecutor commandExecutor) {
        this.commandExecutor = commandExecutor;
    }

    /**
     * This method will be used to import player's data
     * from a flat file to a database (SQLite, MySQL).
     *
     * @param commandSender the {@link CommandSender}
     * @param args          the arguments with th path as first argument
     */
    void handle(final CommandSender commandSender, final List<String> args) {
        if (!commandSender.hasPermission("bungeebot.import")) {
            commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "&c&lAvatarBot &4&l» &cVous ne pouvez pas faire cela.")));
            return;
        }
        if (args.size() == 0) {
            commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "&c&lAvatarSecurity &4&l» &cVous devez renseigner le chemin d'un fichier.")));
            return;
        }
        if (!DBConnection.isOpen()) {
            commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "&c&lAvatarBot &4&l» &cIl n'y a aucune base de données.")));
            return;
        }
        final File file = new File(args.get(0));
        if (!file.exists()) {
            commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "&c&lAvatarBot &4&l» &cLe chemin vers le fichier est incorrect.")));
            return;
        }
        if (!file.isFile()) {
            commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "&c&lAvatarBot &4&l» &cLe chemin n'amène pas vers un fichier.")));
            return;
        }
        commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "&3&lAvatarBot &8&l» &7L'importation va se faire de façon asynchrone.")));
        this.commandExecutor.getMain().getProxy().getScheduler().runAsync(this.commandExecutor.getMain(), () -> {
            try {
                final long time = Calendar.getInstance().getTimeInMillis();
                final List<UUID> uuidList = new ArrayList<>();
                for (final ProxiedPlayer proxiedPlayer : this.commandExecutor.getMain().getProxy().getPlayers()) {
                    if (proxiedPlayer.isConnected())
                        uuidList.add(proxiedPlayer.getUniqueId());
                }
                int success = 0;
                int errors = 0;
                final Configuration configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
                for (final String uuids : configuration.getSection("players").getKeys()) {
                    UUID uuid;
                    try {
                        uuid = UUID.fromString(uuids);
                    } catch (Exception ignored) {
                        errors++;
                        continue;
                    }
                    final String name = configuration.getString("players." + uuid + ".name");
                    final String discordId = configuration.getString("players." + uuid + ".discordID");
                    try (final ResultSet resultSet = DBConnection.sql.readQuery("SELECT * FROM `players_info` WHERE `uuid` = '" + uuid + "'")) {
                        if (!resultSet.next()) {
                            DBConnection.sql.modifyQuery("INSERT INTO `players_info` (`uuid`, `name`, `discordId`, `online`) VALUES ('" + uuid + "', '" + name + "', '" + discordId + "', " + (uuidList.contains(uuid) ? "1" : "0") + ")");
                        } else {
                            DBConnection.sql.modifyQuery("UPDATE `players_info` SET `name` = '" + name + "', `discordId` = '" + discordId + "', `online` = '" + (uuidList.contains(uuid) ? "1" : "0") + "' WHERE `uuid` = '" + uuid + "'");
                        }
                        success++;
                    } catch (SQLException ignored) {
                        errors++;
                    }
                }
                final long now = Calendar.getInstance().getTimeInMillis();
                commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "&3&lAvatarBot &8&l» &7Résultat de l'importation :")));
                commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', " &b-|> &a" + success + " &7personne(s) importée(s),")));
                commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', " &b-|> &c" + errors + " &7erreurs,")));
                commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', " &b-|> &7Durée : &d" + (now - time) + "ms&7.")));
            } catch (IOException ignored) {
                commandSender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "§c§lAvatarBot §4§l» §cImpossible de récupérer la configuration du fichier.")));
            }
        });
    }
}
