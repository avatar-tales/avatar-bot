package fr.bakaaless.AvatarReturnsBot.Bungee.commands;

import fr.bakaaless.AvatarReturnsBot.Bungee.plugin.BungeeMain;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandExecutor extends Command {

    @Getter
    private final BungeeMain main;

    public CommandExecutor(final BungeeMain main) {
        super("bungeebot");
        this.main = main;
        this.getMain().getProxy().getPluginManager().registerCommand(this.getMain(), this);
    }

    @Override
    public void execute(final CommandSender sender, final String[] args) {
        final List<String> arguments = new ArrayList<>(Arrays.asList(args.clone()));
        if (arguments.size() == 0) return;
        final String subCommand = arguments.remove(0);
        if (subCommand.equals("reload"))
            new ReloadCommand(this).handle(sender, arguments);
        else if (subCommand.equalsIgnoreCase("import"))
            new ImportCommand(this).handle(sender, arguments);
        else if (subCommand.equalsIgnoreCase("export"))
            new ExportCommand(this).handle(sender);
        else
            sender.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "&c&lAvatarSecurity &4&l» &cAucune commande trouvée.")));
    }
}
