package fr.bakaaless.AvatarReturnsBot.Bungee.messages;

import fr.bakaaless.AvatarReturnsBot.Bungee.mysql.DBConnection;
import fr.bakaaless.AvatarReturnsBot.Bungee.plugin.BungeeMain;
import lombok.Getter;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BungeeSendMessage {

    @Getter
    private final BungeeMain main;

    public BungeeSendMessage(final BungeeMain main) {
        this.main = main;
        this.getMain().getChannels().add("bungee:bot");
        this.getMain().getChannels().add("BungeeBot");
        this.getMain().getChannels().forEach(channel -> this.getMain().getProxy().registerChannel(channel));
    }

    public void reload(final String... servers) {
        try {
            for (final String serverName : servers) {
                if (!this.getMain().getProxy().getServers().containsKey(serverName)) return;
                final ServerInfo serverInfo = this.getMain().getProxy().getServerInfo(serverName);
                final ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
                final DataOutputStream out = new DataOutputStream(byteArrayOut);
                out.writeUTF("Reload");
                this.getMain().getChannels().forEach(channel -> serverInfo.sendData(channel, byteArrayOut.toByteArray()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void reload(final ServerInfo... servers) {
        try {
            for (final ServerInfo serverInfo : servers) {
                final ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
                final DataOutputStream out = new DataOutputStream(byteArrayOut);
                out.writeUTF("Reload");
                this.getMain().getChannels().forEach(channel -> serverInfo.sendData(channel, byteArrayOut.toByteArray()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void isRegistered(final ProxiedPlayer pp) {
        this.isRegistered(pp, pp.getServer().getInfo());
    }

    public void isRegistered(final ProxiedPlayer pp, final ServerInfo server) {
        try {
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            final DataOutputStream out = new DataOutputStream(byteArrayOutputStream);
            boolean b = false;
            try (final ResultSet resultSet = DBConnection.sql.readQuery("SELECT * FROM `players_info` WHERE `uuid` = '" + pp.getUniqueId().toString() + "'")) {
                if (resultSet.next()) {
                    final String discordId = resultSet.getString("discordId");
                    if (!discordId.equalsIgnoreCase("") && !discordId.equalsIgnoreCase(" ") && resultSet.getString("online").equals("1"))
                        b = true;
                }
            } catch (SQLException ignored) {
            }
            out.writeUTF("responseRegistered");
            out.writeUTF(pp.getName());
            if (b)
                out.writeUTF("true");
            else
                out.writeUTF("false");
            this.main.getChannels().forEach(channel -> server.sendData(channel, byteArrayOutputStream.toByteArray()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
