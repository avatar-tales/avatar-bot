package fr.bakaaless.AvatarReturnsBot.Bungee.events;

import fr.bakaaless.AvatarReturnsBot.Bungee.mysql.DBConnection;
import lombok.Getter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PluginMessageEvent;

import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

class PluginMessageListener {

    @Getter
    private final PluginListener pluginListener;

    PluginMessageListener(final PluginListener pluginListener) {
        this.pluginListener = pluginListener;
    }

    void handle(final PluginMessageEvent e) {
        if (!this.getPluginListener().getMain().getChannels().contains(e.getTag())) return;
        final DataInputStream in = new DataInputStream(new ByteArrayInputStream(e.getData()));
        try {
            final String subChannel = in.readUTF();
            if (subChannel.equalsIgnoreCase("verifyDiscordUser")) {
                final String player = in.readUTF();
                final ProxiedPlayer pp = this.pluginListener.getMain().getProxy().getPlayer(player);
                if (pp == null) return;
                this.pluginListener.getMain().getBungeeSendMessage().isRegistered(pp);
                return;
            } else if (subChannel.equalsIgnoreCase("sendDiscordMessage")) {
                new Thread(() -> {
                    try {
                        final String channelId = in.readUTF();
                        final StringBuilder message = new StringBuilder();
                        while (in.available() > 0) {
                            message.append(in.readUTF()).append(" ");
                        }
                        try {
                            final JDA jda = this.getPluginListener().getMain().getBotDiscord().getJda();
                            for (TextChannel channel : jda.getTextChannels()) {
                                if (channel.getId().equals(channelId)) {
                                    if (message.toString().length() >= 2000) {
                                        if (message.toString().split(System.lineSeparator()).length <= 1) {
                                            for (final String part : message.toString().split("\\.")) {
                                                channel.sendMessage(part + ".").complete();
                                            }
                                        } else {
                                            for (final String part : message.toString().split(System.lineSeparator())) {
                                                channel.sendMessage(part).complete();
                                            }
                                        }
                                        break;
                                    } else
                                        channel.sendMessage(message.toString()).complete();
                                }
                            }
                        } catch (Exception ignored) {
                            System.err.println("Error while sending a message in " + channelId + " : " + message);

                        }
                    } catch (IOException ignored) {

                    }
                }).start();
            }

            if (this.getPluginListener().getMain().getFileManager().getFile("config").getBoolean("factions-integration.enabled")) {
                if (subChannel.equalsIgnoreCase("createFaction")) {
                    new Thread(() -> {
                        try {
                            final String name = in.readUTF();
                            final String player = in.readUTF();
                            final JDA jda = this.getPluginListener().getMain().getBotDiscord().getJda();
                            final Guild guild = jda.getGuildById(this.getPluginListener().getMain().getFileManager().getFile("config").getString("factions-integration.serverID"));
                            assert guild != null;
                            final ProxiedPlayer proxiedPlayer = this.getPluginListener().getMain().getPlayer(player).get();
                            String discordId = "";
                            try (final ResultSet resultSet = DBConnection.sql.readQuery("SELECT * FROM `players_info` WHERE `uuid` = '" + proxiedPlayer.getUniqueId() + "'")) {
                                if (resultSet.next()) {
                                    discordId = resultSet.getString("discordId");
                                }
                            } catch (SQLException ignored) {

                            }
                            final Member member = guild.getMemberById(discordId);
                            final Role roleRegistered = guild.getRoleById(this.getPluginListener().getMain().getFileManager().getFile("config").getString("discord-roles.registeredRoleID"));
                            final Category category = guild.createCategory("〔〕Etat - " + name).complete();
                            final TextChannel publicTextChannel = category.createTextChannel("『\uD83D\uDCAC』discussion-" + name.replace(" ", "-")).complete();
                            final TextChannel privateTextChannel = category.createTextChannel("『\uD83D\uDD12』privé").complete();
                            final VoiceChannel publicVoiceChannel = category.createVoiceChannel("Public").complete();
                            final VoiceChannel privateVoiceChannel = category.createVoiceChannel("Privé").complete();
                            assert member != null;
                            assert roleRegistered != null;
                            publicTextChannel.createPermissionOverride(guild.getPublicRole()).complete().getManager().deny(Permission.VIEW_CHANNEL, Permission.MESSAGE_READ).queue();
                            publicTextChannel.createPermissionOverride(roleRegistered).complete().getManager().grant(Permission.VIEW_CHANNEL, Permission.MESSAGE_READ).queue();
                            publicVoiceChannel.createPermissionOverride(guild.getPublicRole()).complete().getManager().deny(Permission.VIEW_CHANNEL, Permission.VOICE_CONNECT).queue();
                            publicVoiceChannel.createPermissionOverride(roleRegistered).complete().getManager().grant(Permission.VIEW_CHANNEL, Permission.VOICE_CONNECT).queue();
                            final PermissionOverride permissionOverrideText = privateTextChannel.createPermissionOverride(guild.getPublicRole()).complete();
                            permissionOverrideText.getManager().deny(Permission.VIEW_CHANNEL, Permission.MESSAGE_READ, Permission.MESSAGE_WRITE).queue();
                            final PermissionOverride permissionOverrideVoice = privateVoiceChannel.createPermissionOverride(guild.getPublicRole()).complete();
                            permissionOverrideVoice.getManager().deny(Permission.VIEW_CHANNEL, Permission.VOICE_CONNECT).queue();
                            privateVoiceChannel.createPermissionOverride(roleRegistered).complete().getManager().grant(Permission.VIEW_CHANNEL).queue();
                            final Role role = guild.createRole().complete();
                            role.getManager().setColor(new Color(new Random().nextFloat(), new Random().nextFloat(), new Random().nextFloat())).queue();
                            role.getManager().setMentionable(true).queue();
                            role.getManager().setHoisted(false).queue();
                            role.getManager().setName(name).complete();
                            final PermissionOverride permissionOverrideTextRole = privateTextChannel.createPermissionOverride(role).complete();
                            permissionOverrideTextRole.getManager().grant(Permission.VIEW_CHANNEL, Permission.MESSAGE_READ, Permission.MESSAGE_WRITE, Permission.MESSAGE_ATTACH_FILES, Permission.MESSAGE_EMBED_LINKS).queue();
                            final PermissionOverride permissionOverrideVoiceRole = privateVoiceChannel.createPermissionOverride(role).complete();
                            permissionOverrideVoiceRole.getManager().grant(Permission.VIEW_CHANNEL, Permission.VOICE_CONNECT).queue();
                            guild.addRoleToMember(member, role).complete();
                            guild.modifyRolePositions().selectPosition(role).moveUp(3).complete();
                            guild.modifyCategoryPositions().selectPosition(category).moveUp(1).complete();

                        } catch (Exception ignored) {
                            System.err.println("Error while creating a category for");
                        }
                    }).start();
                    return;
                }
                if (subChannel.equalsIgnoreCase("disbandFaction")) {
                    new Thread(() -> {
                        try {
                            final String name = in.readUTF();
                            final JDA jda = this.getPluginListener().getMain().getBotDiscord().getJda();
                            final Guild guild = jda.getGuildById(this.getPluginListener().getMain().getFileManager().getFile("config").getString("factions-integration.serverID"));
                            assert guild != null;
                            final Category category = jda.getCategoriesByName("〔〕Etat - " + name, true).get(0);
                            for (GuildChannel channel : category.getChannels()) {
                                channel.delete().complete();
                            }
                            category.delete().complete();
                            final Role role = guild.getRolesByName(name, true).get(0);
                            for (Member member : guild.getMembers()) {
                                if (member.getRoles().contains(role))
                                    guild.removeRoleFromMember(member, role).complete();
                            }
                            role.delete().complete();
                        } catch (Exception ignored) {
                            System.err.println("Error while deleting a category");
                        }
                    }).start();
                    return;
                }
                if (subChannel.equalsIgnoreCase("renameFaction")) {
                    new Thread(() -> {
                        try {
                            final String name = in.readUTF();
                            final String newName = in.readUTF();
                            final JDA jda = this.getPluginListener().getMain().getBotDiscord().getJda();
                            final Guild guild = jda.getGuildById(this.getPluginListener().getMain().getFileManager().getFile("config").getString("factions-integration.serverID"));
                            assert guild != null;
                            final Role role = guild.getRolesByName(name, true).get(0);
                            role.getManager().setName(newName).queue();
                            final Category category = jda.getCategoriesByName("〔〕Etat - " + name, true).get(0);
                            for (GuildChannel channel : category.getChannels()) {
                                channel.getManager().setName(channel.getName().replace(name, newName)).queue();
                                if (channel instanceof TextChannel) {
                                    channel.getManager().setName(channel.getName().replace(name, newName.replace(" ", "-"))).queue();
                                    channel.getManager().setName(channel.getName().replace(name.replace(" ", "-"), newName.replace(" ", "-"))).queue();
                                }
                            }
                            category.getManager().setName(category.getName().replace(name, newName)).queue();
                        } catch (Exception ignored) {
                            System.err.println("Error while rename a category");
                        }
                    }).start();
                    return;
                }
                if (subChannel.equalsIgnoreCase("joinFaction")) {
                    new Thread(() -> {
                        try {
                            final String name = in.readUTF();
                            final String player = in.readUTF();
                            final JDA jda = this.getPluginListener().getMain().getBotDiscord().getJda();
                            final Guild guild = jda.getGuildById(this.getPluginListener().getMain().getFileManager().getFile("config").getString("factions-integration.serverID"));
                            assert guild != null;
                            final ProxiedPlayer proxiedPlayer = this.getPluginListener().getMain().getPlayer(player).get();
                            String discordId = "";
                            try (final ResultSet resultSet = DBConnection.sql.readQuery("SELECT * FROM `players_info` WHERE `uuid` = '" + proxiedPlayer.getUniqueId() + "'")) {
                                if (resultSet.next()) {
                                    discordId = resultSet.getString("discordId");
                                }
                            } catch (SQLException ignored) {

                            }
                            final Member member = guild.getMemberById(discordId);
                            final Role role = guild.getRolesByName(name, true).get(0);
                            assert member != null;
                            guild.addRoleToMember(member, role).queue();
                        } catch (Exception ignored) {
                            System.err.println("Error while add player to a category");
                        }
                    }).start();
                    return;
                }
                if (subChannel.equalsIgnoreCase("leaveFaction")) {
                    new Thread(() -> {
                        try {
                            final String name = in.readUTF();
                            final String player = in.readUTF();
                            final JDA jda = this.getPluginListener().getMain().getBotDiscord().getJda();
                            final Guild guild = jda.getGuildById(this.getPluginListener().getMain().getFileManager().getFile("config").getString("factions-integration.serverID"));
                            assert guild != null;
                            final ProxiedPlayer proxiedPlayer = this.getPluginListener().getMain().getPlayer(player).get();
                            String discordId = "";
                            try (final ResultSet resultSet = DBConnection.sql.readQuery("SELECT * FROM `players_info` WHERE `uuid` = '" + proxiedPlayer.getUniqueId() + "'")) {
                                if (resultSet.next()) {
                                    discordId = resultSet.getString("discordId");
                                }
                            } catch (SQLException ignored) {

                            }
                            final Member member = guild.getMemberById(discordId);
                            final Role role = guild.getRolesByName(name, true).get(0);
                            assert member != null;
                            guild.removeRoleFromMember(member, role).complete();
                        } catch (Exception ignored) {
                            System.err.println("Error while remove player from a category #1");
                        }
                    }).start();
                    return;
                }
                if (subChannel.equalsIgnoreCase("kickFaction")) {
                    new Thread(() -> {
                        try {
                            final String name = in.readUTF();
                            final String player = in.readUTF();
                            final JDA jda = this.getPluginListener().getMain().getBotDiscord().getJda();
                            final Guild guild = jda.getGuildById(this.getPluginListener().getMain().getFileManager().getFile("config").getString("factions-integration.serverID"));
                            assert guild != null;
                            String discordId = "";
                            try (final ResultSet resultSet = DBConnection.sql.readQuery("SELECT * FROM `players_info` WHERE `name` = '" + name + "'")) {
                                if (resultSet.next()) {
                                    discordId = resultSet.getString("discordId");
                                }
                            } catch (SQLException ignored) {

                            }
                            final Member member = guild.getMemberById(discordId);
                            final Role role = guild.getRolesByName(name, true).get(0);
                            assert member != null;
                            guild.removeRoleFromMember(member, role).complete();
                        } catch (Exception ignored) {
                            System.err.println("Error while remove player from a category #2");
                        }
                    }).start();
                }
            }
        } catch (IOException ignored) {

        }
    }
}
