package fr.bakaaless.AvatarReturnsBot.Bungee.events;

import fr.bakaaless.AvatarReturnsBot.Bungee.mysql.DBConnection;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.Title;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

class PostJoinListener {

    @Getter
    private final PluginListener pluginListener;

    PostJoinListener(final PluginListener pluginListener) {
        this.pluginListener = pluginListener;
    }

    @SuppressWarnings({"deprecation"})
    void handle(final PostLoginEvent e) {
        final ProxiedPlayer pp = e.getPlayer();
        if (this.getPluginListener().getMain().getFileManager().getFile("config").getBoolean("ip.enabled") && this.getPluginListener().getMain().getFileManager().getFile("config").getInt("ip.limit") != -1) {
            if (!this.getPluginListener().getMain().getFileManager().getFile("config").getStringList("ip.bypass").contains(pp.getUniqueId().toString()) && !this.getPluginListener().getMain().getFileManager().getFile("config").getStringList("ip.bypass").contains(pp.getAddress().getHostString())) {
                if (this.getPluginListener().getMain().getFileManager().getFile("ips").get(pp.getAddress().getHostString().replace(".", "*")) == null) {
                    final List<String> uuids = new ArrayList<>();
                    this.getPluginListener().getMain().getFileManager().setLineConfig("ips", pp.getAddress().getHostString().replace(".", "*"), uuids);
                } else if (!this.getPluginListener().getMain().getFileManager().getFile("ips").getStringList(pp.getAddress().getHostString().replace(".", "*")).contains(pp.getUniqueId().toString())) {
                    if (this.getPluginListener().getMain().getFileManager().getFile("config").getInt("ip.limit") < this.getPluginListener().getMain().getFileManager().getFile("ips").getStringList(pp.getAddress().getHostString().replace(".", "*")).size() + 1) {
                        pp.disconnect(TextComponent.fromLegacyText(this.getPluginListener().getMain().getFileManager().getError("ip").replace("%limit%", String.valueOf(this.getPluginListener().getMain().getFileManager().getFile("config").getInt("ip.limit")))));
                        return;
                    }
                }
            }
        }
        final List<String> uuids = this.getPluginListener().getMain().getFileManager().getFile("ips").getStringList(pp.getAddress().getHostString().replace(".", "*"));
        if (!uuids.contains(pp.getUniqueId().toString())) uuids.add(pp.getUniqueId().toString());
        this.getPluginListener().getMain().getFileManager().setLineConfig("ips", pp.getAddress().getHostString().replace(".", "*"), uuids);

        final String serverName = this.getPluginListener().getMain().getFileManager().getFile("config").getString("discord-redirection.redirection-server");
        this.getPluginListener().getMain().getFileManager().setLineConfig("players", "players." + pp.getUniqueId().toString() + ".name", pp.getName());
        if (this.getPluginListener().getMain().getBotDiscord().isStarted() && this.getPluginListener().getMain().getFileManager().getFile("config").getBoolean("discord-redirection.enabled")) {
            String discordId = " ";
            try (final ResultSet resultSet = DBConnection.sql.readQuery("SELECT * FROM `players_info` WHERE `uuid` = '" + pp.getUniqueId().toString() + "'")) {
                if (resultSet.next()) {
                    discordId = resultSet.getString("discordId");
                    DBConnection.sql.modifyQuery("UPDATE `players_info` SET `name` = '" + pp.getName() + "', `online` = '1' WHERE `uuid` = '" + pp.getUniqueId().toString() + "'");
                } else {
                    DBConnection.sql.modifyQuery("INSERT INTO `players_info` (`uuid`,`name`,`discordId`,`online`) VALUES ('" + pp.getUniqueId().toString() + "','" + pp.getName() + "',' ','1')");
                }
            } catch (SQLException ignored) {
                pp.sendMessage(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "&c&lAvatarSecurity &4&l» &cCommunication avec la base de données impossible. Pour des raisons de sécurité, veuillez contacter un administrateur pour pouvoir vous connecter.")));
            }
            this.pluginListener.getMain().getDiscordIds().putIfAbsent(pp, discordId);
            if (discordId.equalsIgnoreCase("") || discordId.equalsIgnoreCase(" ") || !this.getPluginListener().getMain().getBotDiscord().getServerContainsPlayerID(discordId)) {
                if (discordId.equalsIgnoreCase("") || discordId.equalsIgnoreCase(" "))
                    pp.sendMessage(TextComponent.fromLegacyText(this.getPluginListener().getMain().getFileManager().getError("redirection.link")));
                else
                    pp.sendMessage(TextComponent.fromLegacyText(this.getPluginListener().getMain().getFileManager().getError("redirection.already")));
                pp.sendMessage(TextComponent.fromLegacyText(this.getPluginListener().getMain().getFileManager().getMessage("link.accounts")));
                final Title title = this.pluginListener.getMain().getProxy().createTitle();
                title.title(TextComponent.fromLegacyText("§4- §cAvatar Returns §4-"));
                title.subTitle(TextComponent.fromLegacyText("§8§o- §7§oProcédure d'inscription dans le chat §8§o-"));
                title.stay(100);
                pp.sendTitle(title);
                return;
            }
        }
        try {
            ProxyServer.getInstance().getScheduler().schedule(this.getPluginListener().getMain(), () -> pp.connect(this.getPluginListener().getMain().getProxy().getServerInfo(serverName)), 1L, TimeUnit.SECONDS);
        } catch (Exception ignored) {
        }
    }
}
