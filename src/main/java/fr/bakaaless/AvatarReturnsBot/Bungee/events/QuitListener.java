package fr.bakaaless.AvatarReturnsBot.Bungee.events;

import fr.bakaaless.AvatarReturnsBot.Bungee.mysql.DBConnection;
import lombok.Getter;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;

import java.sql.ResultSet;
import java.sql.SQLException;

class QuitListener {

    @Getter
    private final PluginListener pluginListener;

    QuitListener(final PluginListener pluginListener) {
        this.pluginListener = pluginListener;
    }

    void handle(final PlayerDisconnectEvent e) {
        final ProxiedPlayer pp = e.getPlayer();
        this.pluginListener.getMain().getDiscordIds().remove(pp);
        try (final ResultSet resultSet = DBConnection.sql.readQuery("SELECT * FROM `players_info` WHERE `uuid` = '" + pp.getUniqueId().toString() + "'")) {
            if (resultSet.next()) {
                DBConnection.sql.modifyQuery("UPDATE `players_info` SET `online` = '0' WHERE `uuid` = '" + pp.getUniqueId().toString() + "'");
            }
        } catch (SQLException ignored) {
        }
    }
}
