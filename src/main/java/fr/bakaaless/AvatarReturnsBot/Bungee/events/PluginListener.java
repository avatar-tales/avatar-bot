package fr.bakaaless.AvatarReturnsBot.Bungee.events;

import fr.bakaaless.AvatarReturnsBot.Bungee.plugin.BungeeMain;
import lombok.Getter;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PluginListener implements Listener {

    @Getter
    private final BungeeMain main;

    public PluginListener(final BungeeMain main) {
        this.main = main;
        this.getMain().getProxy().getPluginManager().registerListener(this.getMain(), this);
    }

    @EventHandler
    public void onPluginMessage(final PluginMessageEvent e) {
        new PluginMessageListener(this).handle(e);
    }

    @EventHandler
    public void onPostLogin(final PostLoginEvent e) {
        new PostJoinListener(this).handle(e);
    }

    @EventHandler
    public void onPlayerDisconnect(final PlayerDisconnectEvent e) {
        new QuitListener(this).handle(e);
    }

}
