package fr.bakaaless.AvatarReturnsBot.Bungee.mysql;

import fr.bakaaless.AvatarReturnsBot.Bungee.plugin.BungeeMain;
import fr.bakaaless.AvatarReturnsBot.mysql.DataBase;
import fr.bakaaless.AvatarReturnsBot.mysql.MySQL;
import fr.bakaaless.AvatarReturnsBot.mysql.SQLite;

public class DBConnection {

    public static DataBase sql;

    private static String host;
    private static int port;
    private static String db;
    private static String user;
    private static String pass;
    private static boolean isOpen = false;

    public static void init() {
        final BungeeMain plugin = BungeeMain.getInstance();
        DBConnection.host = plugin.getFileManager().getFile("database").getString("MySQL.host");
        DBConnection.port = plugin.getFileManager().getFile("database").getInt("MySQL.port");
        DBConnection.pass = plugin.getFileManager().getFile("database").getString("MySQL.pass");
        DBConnection.db = plugin.getFileManager().getFile("database").getString("MySQL.db");
        DBConnection.user = plugin.getFileManager().getFile("database").getString("MySQL.user");
        if (plugin.getFileManager().getFile("database").getString("engine").equalsIgnoreCase("mysql")) {
            sql = new MySQL(plugin.getLogger(), "Establishing MySQL Connection...", host, port, user, pass, db);
            if (sql.open() == null) {
                plugin.getLogger().severe("Disabling due to database error");
                plugin.onDisable();
                return;
            }
            isOpen = true;
            plugin.getLogger().info("Database connection established.");
            if (!sql.tableExists("players_info")) {
                plugin.getLogger().info("Creating players_info table");
                final String query = "CREATE TABLE `players_info` (" + "`uuid` varchar(36) NOT NULL," + "`name` varchar(16) NOT NULL," + "`discordId` varchar(127) NOT NULL," + "`online` varchar(1) NOT NULL," + " PRIMARY KEY (uuid))";
                sql.modifyQuery(query, false);
            }
        } else {
            sql = new SQLite(plugin.getLogger(), "Establishing SQLite Connection...", "AvatarBot.db", plugin.getDataFolder().getAbsolutePath());
            if (sql.open() == null) {
                plugin.getLogger().severe("Disabling due to database error");
                plugin.onDisable();
                return;
            }
            isOpen = true;
            plugin.getLogger().info("Database connection established.");
            if (!sql.tableExists("players_info")) {
                plugin.getLogger().info("Creating players_info table");
                final String query = "CREATE TABLE `players_info` (" + "`uuid` varchar(36) NOT NULL," + "`name` varchar(16) NOT NULL," + "`discordId` varchar(127) NOT NULL," + "`online` varchar(1) NOT NULL," + " PRIMARY KEY (uuid))";
                sql.modifyQuery(query, false);
            }
        }
    }

    public static boolean isOpen() {
        return isOpen;
    }
}
