package fr.bakaaless.AvatarReturnsBot.Bungee.plugin;

import fr.bakaaless.AvatarReturnsBot.Bungee.bot.BotDiscord;
import fr.bakaaless.AvatarReturnsBot.Bungee.commands.CommandExecutor;
import fr.bakaaless.AvatarReturnsBot.Bungee.events.PluginListener;
import fr.bakaaless.AvatarReturnsBot.Bungee.messages.BungeeSendMessage;
import fr.bakaaless.AvatarReturnsBot.Bungee.mysql.DBConnection;
import fr.bakaaless.AvatarReturnsBot.Bungee.utils.FileManager;
import lombok.Getter;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class BungeeMain extends Plugin {

    private static BungeeMain INSTANCE;

    @Getter
    private FileManager fileManager;

    @Getter
    private BotDiscord botDiscord;

    @Getter
    private PluginListener pluginListener;

    @Getter
    private List<String> channels;

    @Getter
    private BungeeSendMessage bungeeSendMessage;

    @Getter
    private Map<ProxiedPlayer, String> discordIds;

    @Getter
    private CommandExecutor commandExecutor;

    public static BungeeMain getInstance() {
        return INSTANCE;
    }

    @Override
    public void onEnable() {
        INSTANCE = this;
        this.fileManager = new FileManager("config", "messages", "ips", "database");
        DBConnection.init();
        new Thread(() -> {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.botDiscord = new BotDiscord(this);
            this.pluginListener = new PluginListener(this);
            this.channels = new ArrayList<>();
            this.bungeeSendMessage = new BungeeSendMessage(this);
            this.discordIds = new HashMap<>();
            this.commandExecutor = new CommandExecutor(this);
        }).start();
    }

    @Override
    public void onDisable() {
        this.getProxy().getPluginManager().unregisterCommands(this);
        this.getProxy().getPluginManager().unregisterListeners(this);
        DBConnection.sql.close();
    }

    public Optional<ProxiedPlayer> getPlayer(final String player) {
        return this.getProxy().getPlayers().parallelStream().filter(proxiedPlayer -> proxiedPlayer.getName().equalsIgnoreCase(player)).findFirst();
    }

    public boolean isDiscordIDAlreadyLinked(final String id) {
        try (final ResultSet resultSet = DBConnection.sql.readQuery("SELECT * FROM `players_info` WHERE `discordId` = '" + id + "'")) {
            if (resultSet.next()) {
                resultSet.close();
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    public Optional<ProxiedPlayer> getPlayerByDiscord() {
        return Optional.empty();
    }
}
