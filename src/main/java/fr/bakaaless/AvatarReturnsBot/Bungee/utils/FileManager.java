package fr.bakaaless.AvatarReturnsBot.Bungee.utils;

import com.google.common.io.ByteStreams;
import fr.bakaaless.AvatarReturnsBot.Bungee.plugin.BungeeMain;
import lombok.AccessLevel;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

public class FileManager {

    @Getter(AccessLevel.PRIVATE)
    private static BungeeMain main;

    static {
        FileManager.main = BungeeMain.getInstance();
    }

    @Getter
    private final Map<String, Configuration> files;

    public FileManager(final String... files) {
        this.files = new HashMap<>();
        this.init(files);
    }

    public static File loadResource(final String resource) {
        File resourceFile = new File(FileManager.getMain().getDataFolder(), resource);
        try {
            if (!resourceFile.exists()) {
                resourceFile.createNewFile();
                try (InputStream in = FileManager.getMain().getResourceAsStream(resource);
                     OutputStream out = new FileOutputStream(resourceFile)) {
                    ByteStreams.copy(in, out);
                }
            }
        } catch (Exception e) {
            FileManager.getMain().getLogger().log(Level.SEVERE, "§4§lAvatar Bot » Can't write file " + resource, e);
        }
        return resourceFile;
    }

    public void init(final String... files) {
        for (String file : files) {
            initFile(file);
        }
    }

    public void initFile(final String file) {
        try {
            this.getFiles().put(file, ConfigurationProvider.getProvider(YamlConfiguration.class).load(loadResource(file + ".yml")));
        } catch (IOException e) {
            FileManager.getMain().getLogger().log(Level.SEVERE, "§4§lAvatar Bot » Can't load file " + file, e);
        }
    }

    public void reload() {
        for (final String file : this.getFiles().keySet()) {
            this.reload(file);
        }
    }

    public void reload(String file) {
        try {
            this.getFiles().replace(file, ConfigurationProvider.getProvider(YamlConfiguration.class).load(loadResource(file + ".yml")));
        } catch (Exception e) {
            FileManager.getMain().getLogger().log(Level.SEVERE, "§4§lAvatar Bot » Can't reload file " + file, e);
        }
    }

    public void setLineConfig(final String file, final String path, final Object value) {
        if (!this.getFiles().containsKey(file)) return;
        this.getFiles().get(file).set(path, value);
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(this.getFiles().get(file), new File(FileManager.getMain().getDataFolder(), file + ".yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Configuration getFile(final String file) {
        return this.getFiles().get(file);
    }

    public String getMessage(final String path) {
        final String prefix = this.getFile("messages").getString("messages.prefix.info");
        return ChatColor.translateAlternateColorCodes('&', this.getFile("messages").getString("messages.info." + path).replace("%prefix%", prefix));
    }

    public String getError(final String path) {
        final String prefix = this.getFile("messages").getString("messages.prefix.error");
        return ChatColor.translateAlternateColorCodes('&', this.getFile("messages").getString("messages.error." + path).replace("%prefix%", prefix));
    }

}
